package vn.vnpt.ONEHome.logger.model;

public class AppLogDevice {
    private String name;
    private AppLogEvent logEvent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AppLogEvent getLogEvent() {
        return logEvent;
    }

    public void setLogEvent(AppLogEvent logEvent) {
        this.logEvent = logEvent;
    }
}
