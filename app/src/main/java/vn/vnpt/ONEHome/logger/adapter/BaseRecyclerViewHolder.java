package vn.vnpt.ONEHome.logger.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import butterknife.BindView;
import butterknife.ButterKnife;
import vn.vnpt.ONEHome.logger.R;

public abstract class BaseRecyclerViewHolder extends RecyclerView.ViewHolder {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;
    public static final int VIEW_TYPE_FOOTER = 2;
    public static final int VIEW_TYPE_HEADER = 3;
    public static final int VIEW_TYPE_LOADING = 4;

    public BaseRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void onBind(int position) {
    }

    public static class EmptyViewHolder extends BaseRecyclerViewHolder {
        @BindView(R.id.tv_error_content)
        TextView tvErrorContent;
        @BindView(R.id.iv_empty)
        ImageView imgEmpty;
        @BindView(R.id.ctl_empty_view)
        ConstraintLayout ctlEmptyView;

        public EmptyViewHolder(View itemView) {
            super(itemView);

        }

        public EmptyViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_empty, parent, false));
            ButterKnife.bind(this, itemView);
            tvErrorContent.setText("NO DATA");
        }

        public EmptyViewHolder(ViewGroup parent, String emptyContent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_empty, parent, false));
            ButterKnife.bind(this, itemView);
            tvErrorContent.setText(!TextUtils.isEmpty(emptyContent) ? emptyContent : "NO DATA");
        }


        public void setBackgroundClEmptyView(int resource) {
            if (ctlEmptyView != null) {
                ctlEmptyView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), resource));
            }
        }
    }

    public static class FooterHolder extends BaseRecyclerViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);

        }
    }

    public static class LoadingViewHolder extends BaseRecyclerViewHolder {

        public LoadingViewHolder(View itemView) {
            super(itemView);

        }

        public LoadingViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_loading, parent, false));
        }
    }
}
