package vn.vnpt.ONEHome.logger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.vnpt.ONEHome.logger.R;
import vn.vnpt.ONEHome.logger.model.AppLogDevice;

import static vn.vnpt.ONEHome.logger.adapter.BaseRecyclerViewHolder.VIEW_TYPE_EMPTY;
import static vn.vnpt.ONEHome.logger.adapter.BaseRecyclerViewHolder.VIEW_TYPE_LOADING;
import static vn.vnpt.ONEHome.logger.adapter.BaseRecyclerViewHolder.VIEW_TYPE_NORMAL;

public class DeviceLogAdapter extends RecyclerView.Adapter<BaseRecyclerViewHolder> {
    private Context mContext;
    private ArrayList<AppLogDevice> appLogDevices = new ArrayList<>();
    private boolean loading = true;

    public DeviceLogAdapter(Context mContext) {
        this.mContext = mContext;
    }


    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public boolean isLoading() {
        return loading;
    }

    public void addData(ArrayList<AppLogDevice> appDevices) {
        appLogDevices.addAll(appDevices);
        notifyItemRangeInserted(getItemCount(), appDevices.size() - 1);
    }

    @NonNull
    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOADING:
                return new BaseRecyclerViewHolder.LoadingViewHolder(parent);
            case VIEW_TYPE_NORMAL:
                return new DeviceLogViewHolder(parent);
            default:
                return new BaseRecyclerViewHolder.EmptyViewHolder(parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseRecyclerViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (appLogDevices == null || appLogDevices.size() == 0) return 1;
        return appLogDevices.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (appLogDevices == null || appLogDevices.size() == 0) {
            if (isLoading()) return VIEW_TYPE_LOADING;
            else return VIEW_TYPE_EMPTY;
        }
        return VIEW_TYPE_NORMAL;
    }

    public class DeviceLogViewHolder extends BaseRecyclerViewHolder {
        @BindView(R.id.tvDeviceName)
        TextView tvDeviceName;

        public DeviceLogViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);

            AppLogDevice appLogDevice = appLogDevices.get(position);
            tvDeviceName.setText(appLogDevice.getName());

        }
    }
}
