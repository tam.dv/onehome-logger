package vn.vnpt.ONEHome.logger.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.vnpt.ONEHome.logger.R;
import vn.vnpt.ONEHome.logger.adapter.DeviceLogAdapter;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.rvData)
    RecyclerView rvDeviceLog;

    private DeviceLogAdapter mDeviceLogAdapter;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mDeviceLogAdapter = new DeviceLogAdapter(this);
        rvDeviceLog.setLayoutManager(new LinearLayoutManager(this));
        rvDeviceLog.setAdapter(mDeviceLogAdapter);

        mDatabase =  FirebaseDatabase.getInstance().getReference();
        
    }

}